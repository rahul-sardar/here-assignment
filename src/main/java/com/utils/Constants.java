package com.utils;

import java.util.*;

public class Constants {
     
	public static final String MOVIE_PAGE_TITLE = "AllMovie | Movies and Films Database | Movie Search, Ratings, Photos, Recommendations, and Reviews";
	public static final String MAPS_PAGE_TITLE = "Google Maps";
	public static final String WANKHADE_PAGE_TITLE = "Wankhede Stadium Mumbai (Cricket stadium) - Google Maps";
	public static final String WEBTABLE_PAGE_TITLE = "DataTables | Table plug-in for jQuery";
	public static final String PEPPER_FLY_TITLE= "Online Furniture Shopping Store: Shop Online in India for Furniture, Home Decor, Homeware Products @ Pepperfry";
	public static final String SORT_COLUMNNAME = "Age";
	
	public static final String PHONE_NUMBER = "022 2279 5500";
	public static final String SEARCH_DATA = "Wankhede Stadium";
	public static final String LINK_TO_VERIFY = "mumbaicricket.com";
	
	public static final String MOVIE_NAME = "The Godfather";
	public static final String MOVIE_GENRES = "Crime";
	public static final String MOVIE_RATING = "A";
	public static final String MOVIE_DIRECTOR_NAME = "Francis Ford Coppola";
	public static final String MOVIE_CAST_NAME = "Al Pacino";
	public static final String MOVIE_CASTROLE_NAME = "Michael Corleone";
	
	public static final String HOME_ADDRESS = "O T Section, Krishna Nagar, Ulhasnagar-421002, Maharashtra";
	public static final String OFFICE_ADDRESS = "Capgemini Knowledge Park, M6, Capgemini Knowledge Park, Off, Thane - Belapur Rd, TTC Industrial Area, Airoli, Navi Mumbai, Maharashtra 400708";

	
	
	
	
}
