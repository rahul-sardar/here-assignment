package com.base;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.pages.MapPage;
import com.pages.MapSearchHome;
import com.pages.MovieSearch;
import com.pages.Pepperfly;
import com.pages.WebTables;






public class BaseTest {
	
	public BasePage basePage;
	public MapPage mapPage;
	public MapSearchHome mapHomeSearch;
	public MovieSearch movieSearch;
	public WebTables webTables;
	public Pepperfly pepperfry;
	public Properties prop;
	public WebDriver driver;
	
	
	
	@BeforeTest
	@Parameters({"browser", "url"})
	public void setup(String browser, String url) {
		basePage = new BasePage();
		prop = basePage.init_prop();
		driver= basePage.init_driver(browser, prop);
		mapPage = new MapPage(driver);
		mapHomeSearch = new MapSearchHome(driver);
		movieSearch = new MovieSearch(driver);
		webTables = new WebTables(driver);
		pepperfry = new Pepperfly(driver);
		driver.get(url);
	}
	
	@AfterTest
	public void tearDown() {
		driver.close();
	}
	

}
