package com.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;



import io.github.bonigarcia.wdm.WebDriverManager;

public class BasePage {


	public WebDriver driver;
	public Properties prop;


	public static ThreadLocal<WebDriver> tldriver = new ThreadLocal<WebDriver>();
	/**
	 * This Method is used to initialise our WebDriver
	 * @param browser It take Browser name as parameter
	 * @return it return WebDriver Reference
	 */
	
	public WebDriver init_driver(String browser, Properties prop) {
		System.out.println("Browser Value is " +browser);



		if(browser.equalsIgnoreCase("chrome")) {
			WebDriverManager.chromedriver().setup();


			tldriver.set(new ChromeDriver());
			//driver = new ChromeDriver();


		}else if(browser.equalsIgnoreCase("firefox")) {
			WebDriverManager.firefoxdriver().driverVersion("0.29.1").setup();


			tldriver.set(new FirefoxDriver());
			//driver = new FirefoxDriver();


		}else if(browser.equalsIgnoreCase("internetexplorer")) {
			WebDriverManager.iedriver().setup();;


			tldriver.set(new InternetExplorerDriver());
		}else {
			System.out.println("Please pass valid browser as a paramater " +browser);
		}
		//		 driver.manage().deleteAllCookies();
		//		 driver.manage().window().maximize();
		//		 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//		 return driver;
		getDriver().manage().deleteAllCookies();
		getDriver().manage().window().maximize();
		return getDriver();

	}

	public static synchronized WebDriver getDriver() {
		return tldriver.get();
	}
	/**
	 * This Method Used to load properties from config.properties file
	 * @return it return properties prop reference
	 */
	public Properties init_prop() {
		prop = new Properties();

		try {
			FileInputStream ip = new FileInputStream("/Users/rahulmac/eclipse-workspace/HereAssignment/src/main/java/com/config/config.properties");
			prop.load(ip);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return prop; 
	}

	public String getScreenShot(String name) {
		File src = ((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.FILE);
		String path = System.getProperty("user.dir") + "/screenshots/" +System.currentTimeMillis() + name + ".png";
		File destination = new File(path);
		try {
			FileUtils.copyFile(src, destination);
		}catch(IOException e) {
			e.printStackTrace();
		}
		return path;
	}


}
