package com.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.base.BasePage;
import com.utils.Constants;
import com.utils.ElementUtils;

public class Pepperfly extends BasePage{


	private WebDriver driver;
	private ElementUtils utility;

	//1.By Locators
	private By searchField= By.id("search");
	private By searchIcon = By.id("searchButton");
	private By noResults = By.xpath("//span[text() ='No results found for ']");
	private By pricesDisplayed = By.xpath("//span[@class ='clip-offr-price ']");
	private By sortDropDown= By.cssSelector("#curSortType");
	//private By dropDownOptions = By.xpath("//ul[@id='sortBY']//li//a");
	private By linkText = By.linkText("Price Low To High");
	private By framesBox = By.xpath("//iframe[@id='webengage-engagement-callback-frame']");
	private By cross = By.cssSelector("#webklipper-publisher-widget-container-notification-close-div");
	private By closePopUp = By.xpath("//div[@id='regPopUp']//a[@class='popup-close']");
	String beforeXpath= "//table[@id='example']//tbody//tr[";
	String AfterXapth = "]//td[";		
	boolean flag=false;

	//constructor
	public Pepperfly(WebDriver driver) {
		this.driver = driver;
		utility = new ElementUtils(this.driver);

	}

	public String getPageTitle() {
		return utility.waitForTitlePresent(Constants.PEPPER_FLY_TITLE, 10);
	}

	public boolean searchItem(String itemName) {
		try {
			Thread.sleep(5000);
			utility.waitForElementToBeLocated(framesBox, 20);
			driver.switchTo().frame("notification-frame-~197154992");
			driver.findElement(cross).click(); 
		}catch (Exception e) {

		}
		driver.switchTo().defaultContent();
		try {
			utility.waitForElementToBeLocated(closePopUp, 20);
			utility.doClick(closePopUp); 
		}catch (Exception e) {
			// TODO: handle exception
		}
		utility.waitForElementToBeLocated(searchField, 20);
		utility.doSendKeys(searchField, itemName);
		utility.doClick(searchIcon);  
		try {
			//Thread.sleep(5000);
			flag= utility.doIsDisplayed(noResults);
		}catch (Exception e) {
			// TODO: handle exception
		}
		return flag;

	}

	public String [] priceItemsBeforeSort() {
		List<WebElement> list = utility.getElements(pricesDisplayed);
		String[] beforeSortPrice = new String[list.size()];

		for(int i=0; i<list.size(); i++) {
			beforeSortPrice[i]= list.get(i).getText().trim().replace("₹ ","");
		}
		System.out.println("Before Sorting Prices Displayed as---");
		for(String a:beforeSortPrice ) {
			System.out.println(a);
		}
		return beforeSortPrice;

	}

	public String [] priceItemsAfterSortDisplayed() {
		
		utility.waitForElementToBeLocated(sortDropDown, 20);
		utility.doClick(sortDropDown); 
		utility.waitForElementToBeLocated(linkText, 20);
		utility.doClick(linkText); 
		//utility.selectDropDownValueWithoutSelectClass(dropDownOptions, "Price Low to High");
		utility.waitForElementToBeLocated(pricesDisplayed, 20);

		try {
			utility.waitForElementToBeLocated(closePopUp, 20);
			utility.doClick(closePopUp); 
		}catch (Exception e) {
			// TODO: handle exception
		}
		List<WebElement> list = utility.getElements(pricesDisplayed);

		String[] afterSortPrice = new String[list.size()];
		for(int i=0; i<list.size(); i++) {
			afterSortPrice[i]= list.get(i).getText().trim().replace("₹ ","");
		}
		System.out.println("After Sorting Prices Displayed as---");
		for(String a:afterSortPrice ) {
			System.out.println(a);
		}
		return afterSortPrice;


	}


}
