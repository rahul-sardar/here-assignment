package com.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.base.BasePage;
import com.utils.ElementUtils;

public class MapSearchHome extends BasePage {

	private WebDriver driver;
	private ElementUtils utility;

	//1.By Locators
	private By searchBox= By.className("sbib_b");
	private By searchIcon = By.id("searchbox-searchbutton");
	private By time = By.xpath("(//div[@id='section-directions-trip-0']//span)[1]");
	private By searchresult = By.xpath("//div[@class='x3AX1-LfntMc-header-title-ma6Yeb-haAclf']//h2//span");
	private By distance = By.xpath("//div[@id='section-directions-trip-0']//div[contains(@class,'xB1mrd-T3iPGc-trip-tUvA6e')]//div");
	private By address = By.xpath("(//div[@class='rogA2c']/div)[1]");

	private By direction = By.xpath("//div[text()='Directions']"); 
	private By yourLocationSearchTextBox = By.id("sb_ifc51");
	private By yourLocationSearchicon = By.xpath("//div[@id='directions-searchbox-0']//button[@data-tooltip='Search']");

	//constructor
	public MapSearchHome(WebDriver driver) {
		this.driver = driver;
		utility = new ElementUtils(this.driver);

	}

	public void searchText(String text) {

		utility.waitForElementToBeLocated(searchBox, 20);
		utility.doActionsSendKeys(searchBox, text);
		utility.doClick(searchIcon);

	}

	public void printSearchResult(String header) {
        String locator = "//h1//span[text()='" +header + "']";
		utility.waitForElementToBeLocated(By.xpath(locator), 20);
		List<WebElement> list =utility.getElements(searchresult);
		System.out.println("Address Displayed on Left Frame is :");
		for (WebElement e : list) {
			String text = e.getText();
			System.out.println(text);
		}
		getScreenShot("SearchResult");


	}
	
	public void searchOfficeToHome(String officeAdrress) {
		utility.doClick(direction);
		utility.waitForElementToBeLocated(yourLocationSearchTextBox, 20);
		utility.doActionsSendKeys(yourLocationSearchTextBox, officeAdrress);
		utility.doClick(yourLocationSearchicon);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		utility.waitForElementToBeLocated(time, 20);
		String timeRequired = utility.doGetText(time);
		String distanceToCover = utility.doGetText(distance);
		System.out.println("Time Required is " +timeRequired);
		System.out.println("Distance needed to cover is " +distanceToCover);



	}



}
