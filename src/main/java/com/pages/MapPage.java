package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.base.BasePage;
import com.utils.Constants;
import com.utils.ElementUtils;



public class MapPage extends BasePage {

	private WebDriver driver;
	private ElementUtils utility;

	//1.By Locators
	private By searchBox= By.className("sbib_b");
	private By searchIcon = By.id("searchbox-searchbutton");
	private By stadiumText = By.xpath("//div[@class='gm2-body-2']//button[@class='widget-pane-link']");
	private By reviews = By.xpath("(//button[@class='widget-pane-link'])[1]");
	private By ratings = By.xpath("//span[@class='aMPvhf-fI6EEc-KVuj8d']");
	private By address = By.xpath("(//div[@class='rogA2c']/div)[1]");
	
	private By phoneNumber = By.xpath("(//button[@class='widget-pane-link']//following-sibling::span[@class='widget-pane-link'])[last()]"); 
	private By linkText = By.cssSelector(".rogA2c.HY5zDd>div:nth-of-type(1)");

	//constructor
	public MapPage(WebDriver driver) {
		this.driver = driver;
		utility = new ElementUtils(this.driver);

	}

	public void searchText(String text) {
		
		utility.waitForElementToBeLocated(searchBox, 20);
		utility.doActionsSendKeys(searchBox, text);
		utility.doClick(searchIcon);
		getScreenShot(text);
		

	}

	public String getLoginPageTitle() {
		return utility.waitForTitlePresent(Constants.MAPS_PAGE_TITLE, 10);
	}

	public String getLoginPageTitleAfterSearch(String text) {
		return utility.waitForTitlePresent(text, 10);
	}
	
	public String stadiumText() {
		 utility.waitForElementToBeLocated(stadiumText, 20);
		 return driver.findElement(stadiumText).getText();
		 
	}
	
	public String linkText() {
		 utility.scrollIntoView(driver, driver.findElement(linkText));
		 utility.waitForElementToBeLocated(linkText, 20);
		 return utility.doGetText(linkText);
		 
	}
	
	public void printReviewsandRating() {
		 utility.waitForElementToBeLocated(reviews, 20);
		 System.out.println("******************************************************************");
		 System.out.println("Number of Reviews Currently Displayed is " +utility.doGetText(reviews));
		 System.out.println("Ratings Currently Displayed is " +utility.doGetText(ratings));
		 System.out.println("Address Currently Displayed is " +utility.doGetText(address));
		 System.out.println("******************************************************************");
		 
	}
	
	public String getPhoneNumber() {
		 utility.waitForElementToBeLocated(phoneNumber, 20);
		 utility.scrollIntoView(driver, driver.findElement(phoneNumber));
		 getScreenShot("MapEndScreenshot");
		 return utility.doGetText(phoneNumber);
		 
	}


}
