package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.base.BasePage;
import com.utils.Constants;
import com.utils.ElementUtils;

public class WebTables extends BasePage {

	private WebDriver driver;
	private ElementUtils utility;

	//1.By Locators
	private By showEntries= By.name("example_length");
	private By rows = By.xpath("//table[@id='example']//tbody//tr");
	private By columns = By.xpath("//table[@id='example']//thead//tr//th");


	String beforeXpath= "//table[@id='example']//tbody//tr[";
	String AfterXapth = "]//td[";		
	String beforeXpathColumnName = "(//*[text()='";
	String afterXpathColumnName = "'])[1]";
	//constructor
	public WebTables(WebDriver driver) {
		this.driver = driver;
		utility = new ElementUtils(this.driver);

	}

	public String getPageTitle() {
		return utility.waitForTitlePresent(Constants.WEBTABLE_PAGE_TITLE, 10);
	}


	public void showEntries() {
		utility.doSelectDropDownByValue(showEntries, "25");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		utility.waitForElementToBeLocated(rows, 20);
		int rowcount= driver.findElements(rows).size();
		int colCount= driver.findElements(columns).size()-1;
		System.out.println("RowCount is" +rowcount +" , Column Count is" +colCount);
		System.out.println("First 25 Entries Are as Follows");
		for(int row=1; row<=rowcount; row++) {
			for (int col =1; col<=colCount; col++) {
				String ActualXpath = beforeXpath +row +AfterXapth+ col+ "]";
				String Value = driver.findElement(By.xpath(ActualXpath)).getText();
				System.out.print(Value + "          ");
			}
			System.out.println();
			System.out.println();
		}

	}

	public int getColumnNumber(String name) {

		int colCount= driver.findElements(columns).size()-1;
		String beforexpath = "//table[@id='example']//thead//tr//th[";
		String afterxpath = "]";
		for(int i=1; i<=colCount; i++) {
			String columnHeading= driver.findElement(By.xpath(beforexpath +i+ afterxpath)).getText();
			if(columnHeading.contains(name))
				return i;
		}
		return 0;

	}

	public void sortColumn(String colname) {

		String ActualPath = beforeXpathColumnName + colname + afterXpathColumnName;
		utility.waitForElementToBeLocated(By.xpath(ActualPath), 20);
		utility.doClick(By.xpath(ActualPath));
		
	}

	public void showEntrieDetailAccordingtoPostionandAge(String Position, int Age) {
		System.out.println("*******************************************************************************");
		System.out.println("Fetching Details for Position as " +Position +" and age below " +Age );
		int col1 = getColumnNumber("Position");
		int col2 = getColumnNumber("Age");
		utility.doSelectDropDownByValue(showEntries, "25");
		utility.waitForElementToBeLocated(rows, 20);
		int rowcount= driver.findElements(rows).size();
		int colCount= driver.findElements(columns).size()-1;

		for(int row=1; row<=rowcount; row++) {
			String positionxpath = beforeXpath +row +AfterXapth+ col1+ "]";  
			String ageXpath = beforeXpath +row +AfterXapth+ col2+ "]";  
			String position = driver.findElement(By.xpath(positionxpath)).getText();
			String age = driver.findElement(By.xpath(ageXpath)).getText();
			if(position.contains(Position) && Integer.parseInt(age)<=Age) {
				for (int col =1; col<=colCount; col++) {
					String ActualXpath = beforeXpath +row +AfterXapth+ col+ "]";
					String Value = driver.findElement(By.xpath(ActualXpath)).getText();
					System.out.print(Value + "          ");
				}
				System.out.println();
			}

		}
		System.out.println("*****************************************************************************");

	}

}
