package com.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.base.BasePage;
import com.utils.Constants;
import com.utils.ElementUtils;

public class MovieSearch extends BasePage{

	private WebDriver driver;
	private ElementUtils utility;

	//1.By Locators
	private By searchBox= By.xpath("//input[@type='search']");
	private By searchIcon = By.xpath("//input[@type='submit']");
	private By resultHeader = By.xpath("//div[@class='results']//h1");
	//private By searchresultLink = By.xpath("//div[@class='results']//li//div[@class='title']//a");
	private By searchresultText = By.xpath("//div[@class='results']//li//div[@class='title']");
	private By genresText = By.xpath("//span[@class='header-movie-genres']//a");
	private By rating = By.xpath("(//div[@class='details']//span)[last()]");

	private By castCrew = By.linkText("Cast & Crew"); 
	private By directorName = By.xpath("//div[@class='director_container']//div[@class ='info']//a");
	private By castName = By.xpath("//div[@class='cast_container']//div[contains(@class,'cast_name')]");
	//private By castRoleName = By.xpath("//div[@class='cast_container']//div[contains(@class,'cast_name')]");

	//constructor
	public MovieSearch(WebDriver driver) {
		this.driver = driver;
		utility = new ElementUtils(this.driver);

	}

	public String getPageTitle() {
		return utility.waitForTitlePresent(Constants.MOVIE_PAGE_TITLE, 20);
	}

	public void searchText(String text) {

		utility.waitForElementToBeLocated(searchBox, 20);
		utility.doActionsSendKeys(searchBox, text);
		utility.doClick(searchIcon);
		utility.waitForElementToBeLocated(resultHeader, 20);
		
		getScreenShot("SearchMovie");
		//List<WebElement> movieListLink = utility.getElements(searchresultLink);
		List<WebElement> movieListText = utility.getElements(searchresultText);
		System.out.println("The Number of Movie Displayed in Search Result is " +movieListText.size());
		for(WebElement e : movieListText) {
		   String title= e.getText();
		   System.out.println(title);
		   if(title.contains(("1972"))){
			   e.click();
			   break;
			   
		   }
		}


	}
	
	public String verifyGenres() {
		utility.waitForElementToBeVisible(genresText, 20);
		return utility.doGetText(genresText);
	}
	
	public String MPAARating() {
		utility.waitForElementToBeLocated(rating, 20);
		return utility.doGetText(rating);
	}
	
	public String directorName() {
		utility.waitForElementToBeLocated(castCrew, 20);
		utility.doClick(castCrew);
		utility.waitForElementToBeLocated(directorName, 20);
		return utility.doGetText(directorName);
	}
	
	public String characterName(String name) {
		utility.waitForElementToBeLocated(castCrew, 20);
		utility.doClick(castCrew);
		utility.waitForElementToBeLocated(directorName, 20);
		List<WebElement> castNameList = utility.getElements(castName);
		//List<WebElement> castRoleNameList = utility.getElements(castRoleName);
		for(WebElement e : castNameList) {
		   String title= e.getText();
		   System.out.println(title);
		   if(title.contains(name)) {
			   String locator = "//a[text()='" +name + "']//..//..//div[@class='cast_role']";
			   return driver.findElement(By.xpath(locator)).getText();
		   }
		   
		}
		return null;

	}
	

}
