package com.tests;

import java.util.Arrays;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.base.BaseTest;
import com.utils.ExcelUtil;
import com.utils.Constants;

public class PepperFryTest extends BaseTest {


	@Test(priority=1)
	public void verifyLoginPageTestitlePage() {
		String title = pepperfry.getPageTitle();
		System.out.println("Page Title is "+title);
		Assert.assertEquals(title, Constants.PEPPER_FLY_TITLE);

	}

	@DataProvider
	public Object[][] getData(){
		Object[][] data =ExcelUtil.getTestData("Sheet1");
		return data;
	}

	@Test(priority=2, dataProvider = "getData")
	public void searchandDisplayPriceAfterSortTest(String itemName) {
		boolean flag = pepperfry.searchItem(itemName);

		//		String [] beforesortPrice =pepperfry.priceItemsBeforeSort();
		//		String [] aftersortPrice =pepperfry.priceItemsAfterSortDisplayed();
		if(!flag) {
			String [] beforesortPrice =pepperfry.priceItemsBeforeSort();
			String [] aftersortPrice =pepperfry.priceItemsAfterSortDisplayed();
			String [] tempArray = aftersortPrice;
			Arrays.sort(tempArray);
			Assert.assertNotEquals(beforesortPrice, aftersortPrice);
			Assert.assertEquals(aftersortPrice, tempArray);
			
		}else {

			System.out.println("No Result Displayed for an searched item "+itemName);
			Assert.fail();
		}
	}

}
