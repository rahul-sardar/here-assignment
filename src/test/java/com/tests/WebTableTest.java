package com.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.base.BaseTest;
import com.utils.Constants;

public class WebTableTest extends BaseTest {
	
	@Test(priority=1)
	public void verifyLoginPageTestitlePage() {
		String title = webTables.getPageTitle();
		System.out.println("Page Title is "+title);
		Assert.assertEquals(title, Constants.WEBTABLE_PAGE_TITLE);
		
	}
	
	@Test(priority=2)
	public void ShowAndDisplayEntries() {
		webTables.showEntries();
		
	}
	
	@Test(priority=2)
	public void ShowAndDisplayEntriesofSoftwareEngineer() {
		webTables.sortColumn(Constants.SORT_COLUMNNAME);
		webTables.showEntrieDetailAccordingtoPostionandAge("Software Engineer", 30);;
		
	}
	
	

}
