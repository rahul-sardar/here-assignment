package com.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.base.BaseTest;
import com.utils.Constants;



public class MovieSearchTest extends BaseTest {
	
	@Test(priority=1)
	public void verifyLoginPageTestitlePage() {
		String title = movieSearch.getPageTitle();
		System.out.println("Home Page Title is "+title);
		Assert.assertEquals(title, Constants.MOVIE_PAGE_TITLE);
		
	}

	@Test(priority=2)
	public void doGenresTest()  {
		String text ="The Godfather";
		
		movieSearch.searchText(Constants.MOVIE_NAME);
		
		String genres =movieSearch.verifyGenres() ;
		System.out.println("Genres  is "+genres);
		Assert.assertEquals(genres, Constants.MOVIE_GENRES);
		
	}
	
	@Test(priority=3)
	public void doRatingTest()  {
		String rating =movieSearch.MPAARating() ;
		System.out.println("Rating  is "+rating);
		Assert.assertEquals(rating, Constants.MOVIE_RATING);
		
	}
	
	@Test(priority=4)
	public void directorNameTest()  {
		String name =movieSearch.directorName();
		System.out.println("Director Name  is "+name);
		Assert.assertEquals(name, Constants.MOVIE_DIRECTOR_NAME);
		
	}
	
	@Test(priority=5)
	public void characterRoleNameTest()  {
	
		String roleName =movieSearch.characterName(Constants.MOVIE_CAST_NAME);
		System.out.println(roleName);
		if(roleName != null) {
		Assert.assertEquals(roleName, Constants.MOVIE_CASTROLE_NAME);
		}else {
			System.out.println("Role Name is Not Present");
		}
		
	}
	

}
