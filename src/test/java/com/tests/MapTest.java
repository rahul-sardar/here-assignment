package com.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.base.BaseTest;
import com.utils.Constants;



public class MapTest extends BaseTest {
	
	@Test(priority=1)
	public void verifyLoginPageTestitlePage() {
		String title = mapPage.getLoginPageTitle();
		System.out.println("Login Page Title is "+title);
		Assert.assertEquals(title, Constants.MAPS_PAGE_TITLE);
		
	}
	
	@Test(priority=2)
	public void dosearchandverifyLoginPageTestitlePageAfterSearch()  {
		//String searchData= "Wankhede Stadium";
		mapPage.searchText(Constants.SEARCH_DATA);
		String title = mapPage.getLoginPageTitleAfterSearch(Constants.WANKHADE_PAGE_TITLE);
		System.out.println("Page Title is "+title);
		Assert.assertEquals(title, Constants.WANKHADE_PAGE_TITLE);
		
	}
	
	@Test(priority=4)
	public void verifyLinkTextPresent()  {
		String text =mapPage.linkText();
		Assert.assertEquals(text,Constants.LINK_TO_VERIFY);
		
	}
	
	@Test(priority=3)
	public void verifyStadiumTextandPrintReviewRatingAddress()  {
		mapPage.printReviewsandRating();
		String text =mapPage.stadiumText();
		Assert.assertEquals(text, "Stadium");
		
	}
	

	@Test(priority=5)
	public void verifyPhoneNumber()  {
		String text= mapPage.getPhoneNumber();
		Assert.assertEquals(text, Constants.PHONE_NUMBER);
		
	}

}
