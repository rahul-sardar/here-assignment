package com.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.base.BaseTest;
import com.utils.Constants;

public class MapSearchHomeTest extends BaseTest {
	
	@Test(priority=1)
	public void verifyLoginPageTestitlePage() {
		String title = mapPage.getLoginPageTitle();
		System.out.println("Page Title is "+title);
		Assert.assertEquals(title, Constants.MAPS_PAGE_TITLE);
		
	}
	
	@Test(priority=2)
	public void dosearchandPrintResult()  {
		String header ="O T Section";
		mapHomeSearch.searchText(Constants.HOME_ADDRESS);
		mapHomeSearch.printSearchResult(header);
		
	}
	
	@Test(priority=3)
	public void searchOfficetoHomeTest()  {
		mapHomeSearch.searchOfficeToHome(Constants.OFFICE_ADDRESS);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
